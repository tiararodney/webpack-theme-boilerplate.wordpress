const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');
const compiler = require('./common.webpack.config.js').compiler;

compiler.devtool = 'source-map';

compiler.plugins.push(new UglifyJsPlugin({
    sourceMap: true
}))

compiler.plugins.push(new CopyWebpackPlugin(
    [   
        {   
            context: path.resolve('./src/'),
            from: './**/*',
            to: path.join( path.resolve('./build/') )
        }   
    ],  
    {   
        ignore: ['../sass/', '*.scss', 'index.js']
    }   
))

module.exports = compiler
