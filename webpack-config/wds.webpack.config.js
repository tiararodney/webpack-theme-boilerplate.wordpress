const CopyWebpackPlugin = require('copy-webpack-plugin');
const Jarvis = require("webpack-jarvis");
const compiler          = require('./common.webpack.config.js').compiler
const path              = require('path');
const fs                = require('fs')
const yargs             = require("yargs");
const process           = require('process');

var procArgs = yargs(JSON.parse(process.env.npm_config_argv).original).argv;

var jarvisPort = 9001;
var jarvisHost = "0.0.0.0";
var wdsPort = 9000;

if ( ( "wds" in procArgs ) && ( "port" in procArgs["wds"] ) ) {

    if ( procArgs.wds.port > 0 && procArgs.wds.port < 65535 ) {

        wdsPort = procArgs.wds.port;
    }
}

if ( ( "jarvis" in procArgs ) && ( "port" in procArgs["jarvis"] ) ) {

    if ( procArgs.jarvis.port > 0 && procArgs.jarvis.port < 65535 ) {

        jarvisPort = procArgs.jarvis.port;
    }
}

if ( ( "jarvis" in procArgs ) && ( "host" in procArgs["jarvis"] ) ) {

        jarvisHost = procArgs.jarvis.host;
}

if ( !( "wp" in procArgs ) ) {

    throw new Error('No wordpress specific CLI arguments passed. must supply --wp.$ARG.');
}

var envArgs = procArgs.wp;

if (envArgs['rootdir'] === undefined) {

    throw new Error('No wp-root set.');
}

else if ( envArgs['theme-id'] === undefined ) {

    throw new Error('No wp-theme-id set.');
}

compiler.devtool = 'eval-source-map'

compiler.output.path = path.join( envArgs['rootdir'], 'wp-content/themes/', envArgs['theme-id'], '/js');

compiler.devServer = {
    contentBase: path.join( envArgs[ 'rootdir' ], 'wp-content/themes/', envArgs[ 'theme-id'] ),
    port: wdsPort,
    host: "0.0.0.0",
    disableHostCheck: true,
}

compiler.plugins.push(new CopyWebpackPlugin(
    [
        {
            context: path.resolve('./src/'),
            from: './**/*',
            to: path.join( envArgs[ 'rootdir' ], 'wp-content/themes/', envArgs[ 'theme-id'] )
        }
    ],
    {
        ignore: ['../sass/','*.sass', '*.scss', 'index.js']
    }
))

compiler.plugins.push( new Jarvis(
	{
    	port: jarvisPort,
    	host: jarvisHost
	}
))

module.exports = compiler
