
const WriteFilePlugin   = require('write-file-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const yargs             = require('yargs');
const path = require('path');
require('dotenv').config();
const env = require('dotenv-array')()


var procArgs = yargs(JSON.parse(process.env.npm_config_argv).original).argv;


var SASS__INCLUDEPATHS = !( "SASS__INCLUDEPATHS" in env ) ? [] : env[ "SASS__INCLUDEPATHS" ];

if ( !( SASS__INCLUDEPATHS instanceof Array ) ) {

    SASS__INCLUDEPATHS = [ SASS__INCLUDEPATHS ];
} 

if ( ( "sass" in procArgs ) && ( "includepaths" in procArgs["sass"] ) ) {

    SASS__INCLUDEPATHS = SASS__INCLUDEPATHS.concat( Array.from( procArgs.sass.includepaths.split( ',' ) ) );
}

console.log(SASS__INCLUDEPATHS);

exports.compiler = {
    mode: 'production',
    entry: [
        './src/js/index.js',
        './src/style.scss'
    ],
    output: {
        filename: '[name].js',
        path: path.resolve('./build/js'),
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                            plugins: [],
                            sourceMap: true
                        }
                    }
                ]
            },
            { // sass / scss loader for webpack
                test: /\.(sass|scss)$/,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            minimize: true,
                            url: true,
                            exclude: /node_modules/
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            exclude: /node_modules/,
                            includePaths: SASS__INCLUDEPATHS  
                        }
                    }
                ]
            
            },
            {
                test: /\.(woff2?|ttf|otf|eot|svg)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            useRelativePath: true,
                            publicPath: 'fonts/'
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpg|jpeg)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            useRelativePath: true,
                            publicPath: 'img/'
                        }
                    }
                ]
            }
        ]
  },
  plugins: [
      new WriteFilePlugin(),
      new MiniCssExtractPlugin({
          filename: '../style.css',
      }),  
  ]

}
