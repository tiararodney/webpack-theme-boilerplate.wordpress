# sample-webpack-theme.wordpress

##Requirements
- npm ver. >=3.10
- nodejs ver. >=6.17
- ~2GB RAM

##Getting started

###
```bash
npm run dev --wp.rootdir="$PATHTOWORDPRESS" --wp.theme-id="$THEMEID"
```

###
```bash
npm run build --wp.output-path="$OUTPUTPATH"
```

If the main sass stylesheets requires further includes you can set them the following way:

via CLI argument:
```
npm run build --sass.includepaths="path1,path2"
```
or via .env file:
```
SASS__INCLUDEPATHS=path1,path2
```
